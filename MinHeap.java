import java.util.ArrayList;

public class MinHeap<T> {

	private ArrayList<T> heap = new ArrayList<T>();

	private void heapifyUp(int pos) {
		if (pos != 0) {

			int parent;

			if (pos % 2 == 0) {
				parent = (pos - 1) / 2;
			} else {
				parent = pos / 2;
			}

			T e1 = heap.get(parent);
			T e2 = heap.get(pos);

			if (((Comparable) e1).compareTo((Comparable) e2) > 0) {
				// swap
				T temp = heap.get(parent);
				heap.set(parent, heap.get(pos));
				heap.set(pos, temp);

				heapifyUp(parent);
			}
		}

	}

	private void heapifyDown(int pos) {

		int child1 = pos * 2 + 1;
		int child2 = pos * 2 + 2;

		if (child1 < heap.size() || child2 < heap.size()) {

			if (child1 < heap.size() && child2 < heap.size()) {
				// Max of two children
				T e1 = heap.get(child1);
				T e2 = heap.get(child2);
				int max;
				int min;

				if (((Comparable) e1).compareTo((Comparable) e2) < 0) {
					max = child1;
					min = child2;
				} else {
					max = child2;
					min = child1;
				}

				T e3 = heap.get(pos);
				T e4 = heap.get(max);
				// If parent small
				if (((Comparable) e3).compareTo((Comparable) e4) > 0) {
					// Swap
					T temp = heap.get(pos);
					heap.set(pos, heap.get(max));
					heap.set(max, temp);

					heapifyDown(max);
				}

			} else if (child1 < heap.size()) {
				T e1 = heap.get(pos);
				T e2 = heap.get(child1);
				// If parent small
				if (((Comparable) e1).compareTo((Comparable) e2) > 0) {
					// Swap
					T temp = heap.get(pos);
					heap.set(pos, heap.get(child1));
					heap.set(child1, temp);

					heapifyDown(child1);
				}

			} else if (child2 < heap.size()) {
				T e1 = heap.get(pos);
				T e2 = heap.get(child2);
				// If parent small
				if (((Comparable) e1).compareTo((Comparable) e2) > 0) {
					// Swap
					T temp = heap.get(pos);
					heap.set(pos, heap.get(child2));
					heap.set(child2, temp);

					heapifyDown(child2);
				}
			}
		}

	}

	public void add(T element) {

		heap.add(element);
		heapifyUp(heap.size() - 1);

	}

	public T remove() {

		int index = heap.size() - 1;

		T temp = heap.get(0);
		heap.set(0, heap.get(index));
		heap.set(index, temp);

		heap.remove(index);
		heapifyDown(0);

		return temp;

	}

	public T getMin() {
		return heap.get(0);
	}

	public int size() {
		return heap.size();
	}

}
